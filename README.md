# Závěrečná úloha

Závěrečná úloha se skládá ze tří nezávislých úloh. Každá úloha má několik částí, za které můžete získat body. K úlohám
jsou napsané testy a vaším cílem je vytvořit implementaci podle zadání, která bude splňovat testovací podmínky.

Každá úloha má svůj kód ve vlastním namespace `FinalTask\NazevUlohy`. Kód naleznete v podsložkách složky `src`, podle
názvu úlohy.

Projekt dále obsahuje testy v namespace `FinalTask\Tests`. Testy můžete použít pro lepší pochopení zadání. **Nesmíte je
ale jakkoliv upravovat.**

Ve složce `bin` jsou skripty pro vyhodnocování úloh, o ty se nemusíte vůbec starat a také je nesmíte měnit.

## Příprava

Projekt obsahuje composer skripty s několika příkazy, které vám usnadní některé kroky. Předtím, než začnete pracovat,
nainstalujte závislosti pomocí příkazu:

```bash
$ composer install
```

## Spouštění

Chcete-li si spouštět průběžné řešení, napište si vlastní run.php nebo obdobný spouštěcí skript.

## Testování

Pro spuštění testů můžete použít následující příkaz:

```bash
$ composer test
``` 

Pokud chcete spouštět testy pro konkrétní úlohu nebo konkrétní TestCase můžete použít přímo phpunit:

```bash
$ vendor/bin/phpunit src/Tests/NazevUlohy
$ vendor/bin/phpunit src/Tests/NazevUlohy/KonkretniTest.php
```

## Hodnocení

Každá úloha a její části mají přidělený určitý počet bodů. Výsledné hodnocení bude přiděleno na základě splněných úloh v
testech, může být případně upraveno vyučujícím. Hodnocení jednotlivých úloh a částí se dozvíte pomocí příkazu:

```bash
$ composer evaluate
```

_Pokud by vám náhodou příkaz nefungoval, zkuste přidat práva na spuštění: `chmod +x bin/evaluate.php`_


# Úlohy

## 1) Vyhodnocovací strom

Matematické výrazy lze reprezentovat stromem. Např výraz `(6 - 3) + (2 * (1 + 5)` by vypadal následovně:

![](docs/EvaluationTree/example.png)

Vaším úkolem je upravit hierarchii tříd v `src/EvaluationTree` tak, aby odpovídala následujícímu diagramu:

![](docs/EvaluationTree/class-hierarchy.png)

A zároveň implementovat definované funkce (zejména funkci `evaluate`), tak aby fungovalo vyhodnocení stromu. V případě, že výraz z nějakého důvodu nepůjde vyhodnotit (např. dělení nulou), vyhodí vyhodnocení výjimku `FinalTask\EvaluationTree\EvaluationError`.

Kromě evaluate budete pravděpodobně potřebovat zadefinovat na připravených třídách i další metody, to samozřejmě můžete.


## 2) Vyhledávač nejbližších parkovacích automatů v Praze

Na geoportálu Prahy bývali [otevřená data](https://www.geoportalpraha.cz/cs/data/otevrena-data/seznam).

Jednou z těchto datových sad býval GeoJSON veřejných parkovacích automatů. Ten již k dispozici bohužel není, proto použijeme jeho [náhradu zamčenou čase](https://martin.urbanec.cz/files/DOP_CUR_DOP_ZPS_PARKOMATY_B_-1825942765990798841.geojson).

Vašim úkolem bude vytvořit Symfony Command, který bude z konzole načítat informace o zadané zeměpisné šířce a délce
a vypíše nejbližší (vzdušnou čarou) parkovací automat a k němu několik dalších informací.

Formátovaný výstup bude obsahovat:
* **vzdálenost** vzdušnou čarou uvedenou v kilometrech,
* **městskou část** ve které se parkovací automat nachází,
* **celkový počet parkovacích automatů** v té městské části, ve které se nachází nalezený nejbližší parkovací automat,
* **odkaz na otevření mapy** s polohou nalezeného parkovacího automatu.

Další pokyny:
* Můžete očekávat, že vstup byl zadán správně. Tj. **nemusíte validovat** počet a formát parametrů.
* Stažení JSONu do PHP můžete provést např. pomocí vestavěného `curl`, nebo si můžete nainstalovat libovolnou závislost přes composer.
* Pro výpočet vzdálenosti mezi dvěma zeměpisnými body je potřeba použít _Haversinovu formuli_. Najděte si online, jak vypadá.
  Její implementaci **smíte** (na rozdíl od zbylé části řešení) stáhnout z internetu, tělo této funkce nebude hodnoceno.
* Při výpočtu vzdálenosti uvažujte poloměr Země **6371** km.
* Vypsanou vzdálenost vypisujte v kilometrech formátovanou na právě dvě desetinná místa. Jako oddělovač desetinných míst použijte tečku `.`, jako oddělovač tisíců použijte jednu mezeru ` `.

Ukázka spuštění skriptu a správného výstupu:
```shell
php main.php parking-machine --latitude 14.49 --longitude 50.15
```
```text
Nejbližší parkovací automat je asi 1.90 km daleko, nachází se v městské části P8, v celé městské části P8 je celkem 101 parkovacích automatů, https://maps.google.com/maps?q=50.136812557,14.478579329
```


## 3) Seznam dárků

Implementujte metody:
- `Wish::import` - import dat o přáních jednotlivých uživatelů (uživatel A si přeje předmět B),
- `Fulfillment::import`- import dat o jejich splnění jiným uživatelem (t.j. uživatel C se přihlásí, že obstará dárek B pro uživatele A)
- `Fulfillment::unfulfilledWishes` - vrátí seznam přání (pole instancí `Wish`), které nejsou splněny.
- `Fulfillment::equilibrium` - vrátí `true`, pokud všichni uživatelé splnili alespoň 1 přání každému jinému uživateli. V opačném případě vrací `false`.

#### Formát vstupu

Vstupem obou importovacích metod je řetězec ve formátu obdobném CSV, oddělovač je středník tj.
- přání
    - předmět
    - budoucí vlastník
```
Ponožky;Alois
Svetr se sobem;Bohumil
```
- splnění
    - předmět
    - dárce
    - obdarovaný
```
Ponožky;Bohumil;Alois
Svetr se sobem;Alois;Bohumil
```
