<?php

use FinalTask\Culture\CultureCommand;
use Symfony\Component\Console\Application;

require __DIR__ . '/vendor/autoload.php';

$application = new Application();

$application->add(new CultureCommand());

$application->run();

// TODO use this file to test anything you need 
