<?php


declare(strict_types=1);

namespace App;

use FinalTask\EvaluationTree\Node;
use FinalTask\EvaluationTree\NumberNode;

require_once 'vendor/autoload.php'; // Autoload files using Composer's autoloader