<?php

namespace FinalTask\EvaluationTree;

class AdditionNode extends BinaryNode
{

    public function evaluate(): float
    {
        return $this->left->evaluate() + $this->right->evaluate();
    }
}
