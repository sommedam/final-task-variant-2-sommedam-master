<?php
declare(strict_types=1);
namespace FinalTask\EvaluationTree;

abstract class BinaryNode extends Node
{
    public Node $left;
    public Node $right;
    public function setLeft(Node $node): void
    {
        $this->left = $node;
    }

    public function setRight(Node $node): void
    {
        $this->right = $node;
    }
}
