<?php

namespace FinalTask\EvaluationTree;
use FinalTask\EvaluationTree\EvaluationError;

class DivisionNode extends BinaryNode
{

    public function evaluate(): float
    {
        if($this->right->evaluate() == 0){
            throw new EvaluationError();
        }
        return $this->left->evaluate() / $this->right->evaluate();
    }
}
