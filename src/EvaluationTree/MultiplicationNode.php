<?php

namespace FinalTask\EvaluationTree;

class MultiplicationNode extends BinaryNode
{

    public function evaluate(): float
    {
        return $this->left->evaluate() * $this->right->evaluate();
    }
}
