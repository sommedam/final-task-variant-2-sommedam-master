<?php

namespace FinalTask\EvaluationTree;

abstract class Node
{
    abstract public function evaluate(): float;
}
