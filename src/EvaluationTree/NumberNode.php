<?php

namespace FinalTask\EvaluationTree;

use FinalTask\EvaluationTree\EvaluationError;

class NumberNode extends Node
{
    public float $value;
    public function setValue(float $value)
    {
        $this->value = $value;
    }


    /**
     * @throws EvaluationError
     */
    public function evaluate(): float
    {
        if($this->value==null){
            throw new EvaluationError;
        }
        return $this->value;
    }
}
