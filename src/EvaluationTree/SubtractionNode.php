<?php

namespace FinalTask\EvaluationTree;

class SubtractionNode extends BinaryNode
{

    public function evaluate(): float
    {
        return $this->left->evaluate() - $this->right->evaluate();
    }
}
