<?php

namespace FinalTask\Tests\EvaluationTree;

use FinalTask\EvaluationTree\AdditionNode;
use FinalTask\EvaluationTree\BinaryNode;
use FinalTask\EvaluationTree\DivisionNode;
use FinalTask\EvaluationTree\MultiplicationNode;
use FinalTask\EvaluationTree\Node;
use FinalTask\EvaluationTree\NumberNode;
use FinalTask\EvaluationTree\SubtractionNode;
use PHPUnit\Framework\TestCase;

class ClassHierarchyTest extends TestCase
{
    public function testNumberNode()
    {
        $node = new NumberNode();
        $this->assertInstanceOf(Node::class, $node);
    }

    /** @dataProvider operationNodes */
    public function testOperationNode($nodeClass): void
    {
        $node = new $nodeClass();
        $this->assertInstanceOf(BinaryNode::class, $node);
        $this->assertInstanceOf(Node::class, $node);
    }

    public function operationNodes(): array
    {
        return [
            [AdditionNode::class],
            [DivisionNode::class],
            [MultiplicationNode::class],
            [SubtractionNode::class]
        ];
    }
}
