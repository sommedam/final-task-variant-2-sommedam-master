<?php

namespace FinalTask\Tests\EvaluationTree;

use FinalTask\EvaluationTree\AdditionNode;
use FinalTask\EvaluationTree\DivisionNode;
use FinalTask\EvaluationTree\MultiplicationNode;
use FinalTask\EvaluationTree\Node;
use FinalTask\EvaluationTree\NumberNode;
use FinalTask\EvaluationTree\SubtractionNode;
use PHPUnit\Framework\TestCase;

class ComplexEvaluationTest extends TestCase
{
    /** @dataProvider examples */
    public function testEvaluate(Node $node, $expected): void
    {
        $this->assertEqualsWithDelta($expected, $node->evaluate(), PHP_FLOAT_EPSILON);
    }

    public function examples()
    {

        return [
            [ // (6 - 3) + (2 * (1 + 5)
                self::addition(
                    self::subtraction(
                        self::number(6),
                        self::number(3)
                    ),
                    self::multiplication(
                        self::number(2),
                        self::addition(
                            self::number(1),
                            self::number(5)
                        )
                    )
                ),
                15
            ], [ // (5 / 2) * 4 - 11
                self::subtraction(
                    self::multiplication(
                        self::division(
                            self::number(5),
                            self::number(2)
                        ),
                        self::number(4)
                    ),
                    self::number(11)
                ),
                -1
            ], [ // 8 * (18 / 2 - 7) + 26
                self::addition(
                    self::multiplication(
                        self::number(8),
                        self::subtraction(
                            self::division(
                                self::number(18),
                                self::number(2)
                            ),
                            self::number(7)
                        )
                    ),
                    self::number(26)
                ),
                42
            ]
        ];
    }

    private static function number($value): NumberNode
    {
        $node = new NumberNode();
        $node->setValue($value);
        return $node;
    }

    private static function addition(Node $left, Node $right)
    {
        return self::binaryNode(AdditionNode::class, $left, $right);
    }

    private static function subtraction(Node $left, Node $right)
    {
        return self::binaryNode(SubtractionNode::class, $left, $right);
    }

    private static function multiplication(Node $left, Node $right)
    {
        return self::binaryNode(MultiplicationNode::class, $left, $right);
    }

    private static function division(Node $left, Node $right)
    {
        return self::binaryNode(DivisionNode::class, $left, $right);
    }

    private static function binaryNode($nodeClass, Node $left, Node $right)
    {
        $addition = new $nodeClass();
        $addition->setLeft($left);
        $addition->setRight($right);
        return $addition;
    }
}
