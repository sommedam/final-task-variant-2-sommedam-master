<?php

namespace FinalTask\Tests\EvaluationTree;

use FinalTask\EvaluationTree\AdditionNode;
use FinalTask\EvaluationTree\DivisionNode;
use FinalTask\EvaluationTree\EvaluationError;
use FinalTask\EvaluationTree\MultiplicationNode;
use FinalTask\EvaluationTree\NumberNode;
use FinalTask\EvaluationTree\SubtractionNode;
use PHPUnit\Framework\TestCase;

class EdgeCasesTest extends TestCase
{
    /** @dataProvider nodes */
    public function testEmptyNode($nodeClass)
    {
        $this->expectException(EvaluationError::class);
        $node = new $nodeClass();
        $node->evaluate();
    }

    public function nodes()
    {
        return [
            [NumberNode::class],
            [AdditionNode::class],
            [DivisionNode::class],
            [MultiplicationNode::class],
            [SubtractionNode::class]
        ];
    }

    public function testDivisionByZero()
    {
        $leftNode = new NumberNode();
        $leftNode->setValue(9);

        $rightNode = new NumberNode();
        $rightNode->setValue(0);

        $evaluationNode = new DivisionNode();
        $evaluationNode->setLeft($leftNode);
        $evaluationNode->setRight($rightNode);

        $this->expectException(EvaluationError::class);
        $evaluationNode->evaluate();
    }
}
