<?php

namespace FinalTask\Tests\EvaluationTree;

use FinalTask\EvaluationTree\AdditionNode;
use FinalTask\EvaluationTree\DivisionNode;
use FinalTask\EvaluationTree\MultiplicationNode;
use FinalTask\EvaluationTree\NumberNode;
use FinalTask\EvaluationTree\SubtractionNode;
use PHPUnit\Framework\TestCase;

class SimpleEvaluationTest extends TestCase
{
    public function testNumberNodeEvaluation(): void
    {
        $node = new NumberNode();
        $node->setValue(12);
        $this->assertEquals(12, $node->evaluate());
    }

    /** @dataProvider operationNodes */
    public function testBinaryNodeEvaluation($nodeClass, $left, $right, $expected): void
    {
        $leftNode = new NumberNode();
        $leftNode->setValue($left);

        $rightNode = new NumberNode();
        $rightNode->setValue($right);

        $evaluationNode = new $nodeClass();
        $evaluationNode->setLeft($leftNode);
        $evaluationNode->setRight($rightNode);

        $this->assertEquals($expected, $evaluationNode->evaluate());
    }

    public function operationNodes(): array
    {
        return [
            [AdditionNode::class, 10, 20, 30],
            [DivisionNode::class, 50, 2, 25],
            [MultiplicationNode::class, 5, 6, 30],
            [SubtractionNode::class, 34, 12, 22]
        ];
    }
}
