<?php

namespace FinalTask\Tests\WishList;

use FinalTask\WishList\Fulfillment;
use FinalTask\WishList\Wish;
use PDO;
use PHPUnit\Framework\TestCase;

abstract class AbstractTest extends TestCase
{
    protected ?PDO $db;

    public function setUp(): void
    {
        $this->db = new PDO('sqlite::memory:');
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        Wish::createTable($this->db);
        Fulfillment::createTable($this->db);
    }

    public function wishes(): array
    {
        return [
            [
                <<<EOF
Audi A8;Alice
Bentley Continental;Bob
Chevrolet Corvette;Cedric
Subaru Impreza;David
EOF
                ,
                [
                    ['Audi A8', 'Alice'],
                    ['Bentley Continental', 'Bob'],
                    ['Chevrolet Corvette', 'Cedric'],
                    ['Subaru Impreza', 'David'],
                ]
            ],
            [
                <<<EOF
World Peace;Alice
Plush Bear;Bob
Plush Elk;Bob
Plush Reindeer;Bob
Plush Dog;Alice
Plush Cat;Bob
Plush Fish;Bob
Plush Pope;Cedric
Coffee Cup;Cedric
Whiteboard Marker;Cedric
EOF
                ,
                [
                    ['World Peace', 'Alice'],
                    ['Plush Bear', 'Bob'],
                    ['Plush Elk', 'Bob'],
                    ['Plush Reindeer', 'Bob'],
                    ['Plush Dog', 'Alice'],
                    ['Plush Cat', 'Bob'],
                    ['Plush Fish', 'Bob'],
                    ['Plush Pope', 'Cedric'],
                    ['Coffee Cup', 'Cedric'],
                    ['Whiteboard Marker', 'Cedric'],
                ]
            ],
            [
                <<<EOF
Bob;Alice Awkward
Alice;Bob "The Builder"
Christmas Tree;Bob Brown
Easter Egg;David O'Donell
Fender Stratocaster;Eleanor
Gibson Les Paul;Eleanor
EOF
                ,
                [
                    ['Bob', 'Alice Awkward'],
                    ['Alice', 'Bob "The Builder"'],
                    ['Christmas Tree', 'Bob Brown'],
                    ['Easter Egg', 'David O\'Donell'],
                    ['Fender Stratocaster', 'Eleanor'],
                    ['Gibson Les Paul', 'Eleanor'],
                ]
            ],
            ['', []],
        ];
    }

    public function fulfillments(): array
    {
        return [
            [
                <<<EOF
Chevrolet Corvette;Bob;Cedric
Bentley Continental;Alice;Bob
Audi A8;Bob;Alice
EOF
                ,
                [
                    ['Chevrolet Corvette', 'Bob', 'Cedric'],
                    ['Bentley Continental', 'Alice', 'Bob'],
                    ['Audi A8', 'Bob', 'Alice'],
                ]
            ],
            [
                <<<EOF
Plush Elk;Cedric;Bob
Whiteboard Marker;Alice;Cedric
World Peace;Cedric;Alice
Plush Bear;Alice;Bob
Plush Dog;Bob;Alice
Coffee Cup;Bob;Cedric
Plush Reindeer;Alice;Bob
EOF
                ,
                [
                    ['Plush Elk', 'Cedric', 'Bob'],
                    ['Whiteboard Marker', 'Alice', 'Cedric'],
                    ['World Peace', 'Cedric', 'Alice'],
                    ['Plush Bear', 'Alice', 'Bob'],
                    ['Plush Dog', 'Bob', 'Alice'],
                    ['Coffee Cup', 'Bob', 'Cedric'],
                    ['Plush Reindeer', 'Alice', 'Bob'],
                ]
            ],
            [
                <<<EOF
Bob;Bob "The Builder";Alice Awkward
Alice;Alice Awkward;Bob "The Builder"
Christmas Tree;David O'Donell;Bob Brown
Easter Egg;Alice Awkward;David O'Donell
EOF
                ,
                [
                    ['Bob', 'Bob "The Builder"', 'Alice Awkward'],
                    ['Alice', 'Alice Awkward', 'Bob "The Builder"'],
                    ['Christmas Tree', 'David O\'Donell', 'Bob Brown'],
                    ['Easter Egg', 'Alice Awkward', 'David O\'Donell'],
                ]
            ],
            ['', []],
        ];
    }

    public function unfullfilled(): array
    {
        return [
            [
                ['Subaru Impreza', 'David'],
            ],
            [
                ['Plush Cat', 'Bob'],
                ['Plush Fish', 'Bob'],
                ['Plush Pope', 'Cedric'],
            ],
            [
                ['Fender Stratocaster', 'Eleanor'],
                ['Gibson Les Paul', 'Eleanor'],
            ],
            [],
        ];
    }

    public function equilibriums(): array
    {
        return [false, true, false, true];
    }

    public function fulfillmentCombo(): array
    {
        $combo = [];
        $wishData = $this->wishes();
        $fulfillmentData = $this->fulfillments();
        $unfulfilled = $this->unfullfilled();
        foreach ($wishData as $setId => $setData) {
            $combo[] = [$setData, $fulfillmentData[$setId], $unfulfilled[$setId]];
        }

        return $combo;
    }

    public function equilibriumCombo(): array
    {
        $equilibriums = $this->equilibriums();
        $combo = [];
        $fulfillmentData = $this->fulfillments();
        foreach ($fulfillmentData as $setId => $setData) {
            $combo[] = [$setData[0], $equilibriums[$setId]];
        }

        return $combo;
    }
}
