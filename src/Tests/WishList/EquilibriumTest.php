<?php

namespace FinalTask\Tests\WishList;

use FinalTask\WishList\Fulfillment;

class EquilibriumTest extends AbstractTest
{
    /** @dataProvider equilibriumCombo */
    public function testEquilibrium($raw, $expected): void
    {
        Fulfillment::import($this->db, $raw);
        $actual = Fulfillment::equilibrium($this->db);
        $this->assertSame($expected, $actual);
    }
}
