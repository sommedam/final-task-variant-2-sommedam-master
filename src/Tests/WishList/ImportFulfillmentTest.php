<?php

namespace FinalTask\Tests\WishList;

use FinalTask\WishList\Fulfillment;

class ImportFulfillmentTest extends AbstractTest
{
    /** @dataProvider fulfillments */
    public function testImport($raw, $data): void
    {
        Fulfillment::import($this->db, $raw);
        $statement = $this->db->query('SELECT object, donor, donee FROM fulfillment');
        $this->assertNotTrue(!$statement);
        $records = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $this->assertCount(count($data), $records);
        foreach ($records as $record) {
            $this->assertContains([$record['object'], $record['donor'], $record['donee']], $data);
        }
    }
}
