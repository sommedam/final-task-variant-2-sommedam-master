<?php

namespace FinalTask\Tests\WishList;

use FinalTask\WishList\Wish;

class ImportWishTest extends AbstractTest
{
    /** @dataProvider wishes */
    public function testImport($raw, $data): void
    {
        Wish::import($this->db, $raw);
        $statement = $this->db->query('SELECT owner, object FROM wish');
        $this->assertNotTrue(!$statement);
        $records = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $this->assertCount(count($data), $records);
        foreach ($records as $record) {
            $this->assertContains([$record['object'], $record['owner']], $data);
        }
    }


}
