<?php

namespace FinalTask\Tests\WishList;

use FinalTask\WishList\Fulfillment;
use FinalTask\WishList\Wish;

class UnfulfilledWishTest extends AbstractTest
{
    /** @dataProvider fulfillmentCombo */
    public function testFulfillments(array $wishes, array $fulfillments, array $expected): void
    {
        Wish::import($this->db, $wishes[0]);
        Fulfillment::import($this->db, $fulfillments[0]);
        $actual = Fulfillment::unfulfilledWishes($this->db);
        $this->assertCount(count($expected), $actual);
        $this->assertContainsOnlyInstancesOf(Wish::class, $actual);
        foreach ($actual as $wish) {
            $this->assertContains([$wish->getObject(), $wish->getOwner()], $expected);
        }
    }
}
