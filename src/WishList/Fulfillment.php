<?php

namespace FinalTask\WishList;

class Fulfillment
{
    public function __construct(

        /** gift or wish title */
        protected string $object,

        /** who gives the gift = fulfill the wish */
        protected string $donor,

        /** recipient of the gift */
        protected string $donee
    )
    {
    }

    public function getObject(): string
    {
        return $this->object;
    }

    public function getDonor(): string
    {
        return $this->donor;
    }

    public function getDonee(): string
    {
        return $this->donee;
    }

    public static function createTable(\PDO $db): void
    {
        $db->query(
            'CREATE TABLE fulfillment (id INTEGER PRIMARY KEY, object VARCHAR(32), donor VARCHAR(32), donee VARCHAR(32))'
        );
    }

    public static function import(\PDO $db, string $data): void
    {
        // TODO implement
    }

    /**
     * @return array|Wish[]
     */
    public static function unfulfilledWishes(\PDO $db): array
    {
        // TODO implement

        return [];
    }

    public static function equilibrium(\PDO $db): bool
    {
        // TODO implement

        return true;
    }
}
