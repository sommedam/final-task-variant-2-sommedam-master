<?php

namespace FinalTask\WishList;

class Wish
{
    public function __construct(

        /** gift or wish title */
        protected string $object,

        /** who has the wish */
        protected string $owner
    )
    {
    }

    public function getObject(): string
    {
        return $this->object;
    }

    public function getOwner(): string
    {
        return $this->owner;
    }

    public static function createTable(\PDO $db): void
    {
        $db->query(
            'CREATE TABLE wish (id INTEGER PRIMARY KEY, object VARCHAR(32), owner VARCHAR(32))'
        );
    }

    public static function import(\PDO $db, string $data): void
    {
        // TODO implement
    }
}
